package com.example.tipcalculator

import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.NumberFormat

class TipCalculatorTests {
    @Test
    fun calculateTip_20PercentNoRoundup() {
        val amount = 10.00
        val tipPercent = 20.00

        val expectedTip = NumberFormat.getCurrencyInstance().format(2)
        val actualTip = calculateTip(amount = amount, tipPercent = tipPercent, false)
        assertEquals(expectedTip, actualTip)
    }

    @Test
    fun calculate_9_99_Tip_30PercentRoundup() {
        val amount = 9.99
        val tipPercent = 30.00
        val roundUp = true

        val expectedTip = NumberFormat.getCurrencyInstance().format(3)
        val actualTip = calculateTip(amount = amount, tipPercent = tipPercent, roundUp)
        assertEquals(expectedTip, actualTip)
    }

    @Test
    fun calculate_10_30_Tip_30PercentRoundup() {
        val amount = 12.00
        val tipPercent = 30.00
        val roundUp = true

        val expectedTip = NumberFormat.getCurrencyInstance().format(4)
        val actualTip = calculateTip(amount = amount, tipPercent = tipPercent, roundUp)
        assertEquals(expectedTip, actualTip)
    }

    @Test
    fun calculateTip_0PercentNoRoundup() {
        val amount = 100.00
        val tipPercent = 0.0

        val expectedTip = NumberFormat.getCurrencyInstance().format(0)
        val actualTip = calculateTip(amount = amount, tipPercent = tipPercent, false)
        assertEquals(expectedTip, actualTip)
    }
}