package com.example.artspace

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.artspace.ui.theme.ArtSpaceTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArtSpaceTheme {
                ArtAppLayout()
            }
        }
    }
}

@Composable
fun ArtAppLayout() {
    var currentStep by remember { mutableIntStateOf(1) }

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        color = Color(0xFFFF813E)
    ) {
        when (currentStep) {
            1 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(20.dp)
                ) {
                    ArtBack(
                        resourceDrawable = R.drawable.art1,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ArtDescription(
                        title = R.string.art1_name,
                        artist = R.string.artist1,
                        year = R.string.art1_year
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        backwardClick = { currentStep = 5 },
                        forwardClick = { currentStep = 2 }
                    )
                }
            }

            2 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(20.dp)
                ) {
                    ArtBack(
                        resourceDrawable = R.drawable.art2,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ArtDescription(
                        title = R.string.art2_name,
                        artist = R.string.artist2,
                        year = R.string.art2_year
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        backwardClick = { currentStep = 1 },
                        forwardClick = { currentStep = 3 })
                }
            }

            3 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(20.dp)
                ) {
                    ArtBack(
                        resourceDrawable = R.drawable.art3,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ArtDescription(
                        title = R.string.art3_name,
                        artist = R.string.artist3,
                        year = R.string.art3_year
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        backwardClick = { currentStep = 2 },
                        forwardClick = { currentStep = 4 })
                }
            }

            4 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(20.dp)
                ) {
                    ArtBack(
                        resourceDrawable = R.drawable.art4,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ArtDescription(
                        title = R.string.art4_name,
                        artist = R.string.artist4,
                        year = R.string.art4_year
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        backwardClick = { currentStep = 3 },
                        forwardClick = { currentStep = 5 })
                }
            }

            5 -> {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(20.dp)
                ) {
                    ArtBack(
                        resourceDrawable = R.drawable.art5,
                        contentDescription = R.string.art_content_description
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ArtDescription(
                        title = R.string.art5_name,
                        artist = R.string.artist5,
                        year = R.string.art5_year
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    ButtonPanel(
                        backwardClick = { currentStep = 4 },
                        forwardClick = { currentStep = 1 })
                }
            }
        }
    }
}


@Composable
fun ArtBack(
    resourceDrawable: Int,
    contentDescription: Int
) {
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .height(500.dp)
            .border(
                BorderStroke(4.dp, Color(0xFFAF6450))
            ),
        color = Color(0xFFB3AE54),
        shadowElevation = 25.dp
    ) {
        Image(
            painter = painterResource(
                id = resourceDrawable
            ),
            contentDescription = stringResource(id = contentDescription),
            modifier = Modifier
                .padding(25.dp)
        )
    }
}

@Composable
fun ArtDescription(
    title: Int,
    artist: Int,
    year: Int
) {
    Surface(
        shadowElevation = 25.dp,
        color = Color(0xFFFFDA96),
        modifier = Modifier
            .border(
                BorderStroke(4.dp, Color(0xFFAF6450))
            ),
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
        ) {
            Text(
                text = stringResource(title),
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center
            )
            Text(
                text = stringResource(artist),
                fontSize = 20.sp,
                fontStyle = FontStyle.Italic
            )
            Text(
                text = stringResource(year),
                fontSize = 15.sp
            )
        }
    }
}

@Composable
fun ButtonPanel(
    backwardClick: () -> Unit,
    forwardClick: () -> Unit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceAround,
        verticalAlignment = Alignment.Bottom
    ) {
        Button(
            onClick = backwardClick,
            colors = ButtonDefaults.buttonColors(Color(0xFFF84242)),
        ) {
            Text(
                text = stringResource(id = R.string.previous_button),
                textAlign = TextAlign.Center,
                modifier = Modifier.width(100.dp),
                fontSize = 20.sp,
                color = Color(0xFFFFFFFF)
            )
        }
        Button(
            onClick = forwardClick,
            colors = ButtonDefaults.buttonColors(Color(0xFF588B42))

        ) {
            Text(
                text = stringResource(id = R.string.next_button),
                textAlign = TextAlign.Center,
                modifier = Modifier.width(100.dp),
                fontSize = 20.sp,
                color = Color(0xFFFFFFFF)
            )
        }
    }
}

@Preview()
@Composable
fun ArtAppPreview() {
    ArtSpaceTheme {
        ArtAppLayout()
    }
}